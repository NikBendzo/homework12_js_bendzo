"use script";


const buttons = document.querySelectorAll(".btn");
let activeKey = null;

function changeButtonColor(letter, enable) {
  buttons.forEach(function (button) {
    if (button.textContent === letter) {
      button.style.backgroundColor = enable ? "blue" : "black";
    }
  });
}

function deactivatePreviousKey() {
  if (activeKey) {
    changeButtonColor(activeKey, false);
  }
}

buttons.forEach(function (button) {
  button.addEventListener("click", function (event) {
    const letter = event.target.textContent;
    deactivatePreviousKey();
    if (activeKey === letter) {
      activeKey = null;
    } else {
      activeKey = letter;
    }
    changeButtonColor(letter, activeKey !== null);
  });
});

document.addEventListener("keydown", function (event) {
  let letter = event.key.toUpperCase();

  if (letter === "ENTER") {
    letter = "Enter";
  }

  deactivatePreviousKey();

  if (activeKey === letter) {
    activeKey = null;
    return;
  }

  activeKey = letter;
  changeButtonColor(letter, true);
});